<?php

/**
 * @file
 * This module holds functions for simple_entity_merge.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_entity_type_alter().
 */
function simple_entity_merge_entity_type_alter(array &$entity_types) {
  $config = \Drupal::config('simple_entity_merge.settings');
  $exclude = $config->get('exclude') ?? 'node_type,block_content_type';
  $entity_exclude = array_map('trim', explode(",", $exclude));

  foreach ($entity_types as $entity_type_id => $entity_type) {
    if (in_array($entity_type_id, $entity_exclude)) {
      continue;
    }
    if (($entity_type->getFormClass('default') || $entity_type->getFormClass('edit')) && $entity_type->hasLinkTemplate('edit-form')) {
      $entity_type->setLinkTemplate('simple_entity_merge-execute', "/simple_entity_merge/$entity_type_id/{{$entity_type_id}}");
      $entity_type->setFormClass('simple_entity_merge', 'Drupal\simple_entity_merge\Form\Merge');
    }
  }
}

/**
 * Implements hook_entity_operation().
 */
function simple_entity_merge_entity_operation(EntityInterface $entity) {
  $operations = [];
  if (\Drupal::currentUser()->hasPermission('execute simple_entity_merge')) {
    if ($entity->hasLinkTemplate('simple_entity_merge-execute')) {
      $operations['simple_entity_merge'] = [
        'title' => t('Simple Entity Merge'),
        'weight' => 100,
        'url' => $entity->toUrl('simple_entity_merge-execute'),
      ];
    }
  }
  return $operations;
}
